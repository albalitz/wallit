#!/usr/bin/env python3
"""
Wallit - WALLpapers from reddIT.

Download top images from subreddits and set them as your wallpapers.
"""
import os
import shutil
import argparse
import urllib.request
import glob
import subprocess
from random import randint
from time import sleep

import praw
from PIL import Image

from wm import Feh as WallpaperManager

_VERBOSE = False

# Check https://www.reddit.com/r/sfwpornnetwork/wiki/network for more.
_SUBREDDITS = [
    "CityPorn",
    "BridgePorn",
    "SpringPorn",
    "SummerPorn",
    "AutumnPorn",
    "WinterPorn",
    "ArchitecturePorn",
    "WaterPorn",
    "SkyPorn",
    "DesertPorn",
    "BeachPorn",
    "LakePorn",
    "EarthPorn",
    "BotanicalPorn"
]

_LIMIT = 10  # How many images you want from every subreddit at most
_TIME_BETWEEN_WALLPAPERS = 10 * 60 # Time in seconds you want to wait until setting the next wallpaper

_HOME = os.path.expanduser("~")
_WALLPAPER_DIRECTORY = os.path.join(_HOME, "Pictures", "Wallpapers")
_FAVORITES_DIRECTORY = os.path.join(_WALLPAPER_DIRECTORY, "Favorites")
_WALLPAPER_DOWNLOAD_LOG = os.path.join(_WALLPAPER_DIRECTORY, "wallit.log")

_MINIMUM_HEIGHT = 1080
_MINIMUM_WIDTH = 1920
_ALLOW_VERTICAL_IMAGES = False

# the command you want to use to set your wallpaper.
_WALLPAPER_COMMAND = "feh --bg-fill --recursive --randomize {}".format(_WALLPAPER_DIRECTORY)

r = praw.Reddit(user_agent="WallIt - Wallpapers from Reddit.")


def favorize_wallpapers(monitors):
    """
    Copy wallpapers from given monitors to a favorites directory.
    """
    os.makedirs(_FAVORITES_DIRECTORY, exist_ok=True)

    kwargs = {
        "wallpaper_directory": _WALLPAPER_DIRECTORY,
        "monitor_count": 3
    }
    current_wallpapers = WallpaperManager(**kwargs).get_current_wallpapers(monitors)
    if _VERBOSE:
        print("Current wallpapers:", current_wallpapers)

    to_be_favorized = [current_wallpapers[monitor] for monitor in monitors]
    for wallpaper in to_be_favorized:
        favorite = wallpaper.replace(_WALLPAPER_DIRECTORY, _FAVORITES_DIRECTORY)
        if _VERBOSE:
            print("Copying: {w} -> {f}...".format(
                w = wallpaper,
                f = favorite
            ))
        shutil.copy(wallpaper, favorite)


def log_filename(filename):
    if _VERBOSE:
        print("Logging filename: {}".format(filename))
    filenames = []
    try:
        with open(_WALLPAPER_DOWNLOAD_LOG, "r") as f:
            filenames = [l.strip() for l in f.readlines()]
    except FileNotFoundError:
        pass  # will be created

    filenames.append(filename)

    with open(_WALLPAPER_DOWNLOAD_LOG, "w+") as f:
        f.write("\n".join(sorted(filenames)))


def is_filename_in_log(filename):
    try:
        with open(_WALLPAPER_DOWNLOAD_LOG, "r") as f:
            return filename in [l.strip() for l in f.readlines()]
    except FileNotFoundError:
        return False


def download_images_from_sub(sub):
    """
    Downloads images from a subreddit's top submissions to your wallpaper directory.

    Currently supported hosts are
      - imgur.com

    :param sub: Name of a subreddit.
    """
    if _VERBOSE:
        print("\nChecking {} for wallpapers...".format(sub))

    subreddit = r.get_subreddit(sub)
    top = subreddit.get_top_from_month(limit=_LIMIT)

    for submission in top:
        url = submission.url

        # todo: other hosts (flickr, 500px, etc.)
        if "imgur.com" in url and not url.endswith(".jpg") and not url.endswith("/"):
            if _VERBOSE:
                print("Trying to get an image from imgur: {}".format(url))
            url += ".jpg"

        if url.endswith(".jpg") or any(h in url for h in ["i.reddituploads.com", "i.redd.it"]):
            image_name = "{}.jpg".format(submission.title.replace("/", "-").replace("\"", "'"))
            destination_file = os.path.join(_WALLPAPER_DIRECTORY, image_name)

            if not os.path.exists(_WALLPAPER_DIRECTORY):
                os.makedirs(_WALLPAPER_DIRECTORY)

            if not is_filename_in_log(image_name):
                if _VERBOSE:
                    print("Downloading image: {} -> {}...".format(url, destination_file))

                try:
                    urllib.request.urlretrieve(url, destination_file)
                    log_filename(image_name)

                    with Image.open(destination_file) as im:
                        width, height = im.size
                        if ((width < _MINIMUM_WIDTH and height < _MINIMUM_HEIGHT)
                            or (not _ALLOW_VERTICAL_IMAGES and height > width)):
                            os.remove(destination_file)

                except urllib.request.HTTPError as he:
                    if _VERBOSE:
                        print("Wasn't able to download from {}: {}".format(url, he))
            else:
                if _VERBOSE:
                    print("Image was already downloaded once. Not downloading again.")


def set_random_wallpaper():
    """
    Uses the set command to change the wallpaper.
    """
    images = glob.glob(os.path.join(_WALLPAPER_DIRECTORY, "**", "*.jpg"), recursive=True)

    wallpaper_command = [i for i in _WALLPAPER_COMMAND.split(" ")]
    if _VERBOSE:
        print("\nUsing '{}' to set a new wallpaper...".format(_WALLPAPER_COMMAND))

    try:
        subprocess.run(wallpaper_command)
    except FileNotFoundError as fnfe:
        print("ERROR while executing the command to change your wallpaper:\n{}".format(fnfe))


def main(**kwargs):
    endless = kwargs.get("endless", False)
    offline = kwargs.get("offline", False)

    while(True):
        if not offline:
            try:
                urllib.request.urlopen("https://reddit.com", timeout=1)

                for sub in _SUBREDDITS:
                    download_images_from_sub(sub)
            except urllib.request.URLError:
                if _VERBOSE:
                    print("Seems like there's no internet. Not trying to download anything.")

        set_random_wallpaper()

        if endless:
            sleep(_TIME_BETWEEN_WALLPAPERS)
        else:
            break


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description="Wallpapers from Reddit.")
    argparser.add_argument("-e", "--endless",
                           action="store_true",
                           help="Repeatedly look for and set wallpapers.")
    argparser.add_argument("-v", "--verbose",
                           action="store_true",
                           help="Verbose output.")
    argparser.add_argument("-o", "--offline",
                           action="store_true",
                           help="Don't download anything, just choose from local files.")
    argparser.add_argument("-f", "--favorize",
                           action="store",
                           metavar="monitor_number",
                           nargs="+",
                           help="Copy the wallpaper(s) from the given monitor(s) to the favorites directory. (1-indexed)")

    args = argparser.parse_args()

    _VERBOSE = args.verbose

    if args.favorize:
        favorize_wallpapers([int(monitor) - 1 for monitor in args.favorize])
        quit()

    main(endless=args.endless, offline=args.offline)
