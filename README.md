Wallit
======
**Wall**papers from Redd**it**.

## Setup
1. Clone this repo.
2. Install the required python packages: `pip install --user -r requirements.txt`
3. Open `wallit.py` in your favorite editor and configure wallit to fit your needs:  
  - Set your favorite subreddits.
  - How many images do you want at most?
  - How often do you want a new wallpaper?
  - Where do you want to save your wallpapers to/choose wallpapers from?
  - What's the command to change wallpapers on your system?
4. Check `wallit.py -h` for a parameter overview.

## But I don't want to run this everytime I want a new wallpaper!
Then don't.

Just run `path/to/wallit.py -e &` in your login script, let wallit download and set new wallpapers for you in the background, and sit back, relax, and enjoy your new fancy wallpapers.
