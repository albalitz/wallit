"""
Wallpaper management functions depending on specific methods of setting wallpapers.
"""

import os
import re


class WallpaperManagement:
    def __init__(self, wallpaper_directory, monitor_count):
        self.wallpaper_directory = wallpaper_directory
        self.monitor_count = monitor_count

    def get_current_wallpapers(self):
        """Get the list of current wallpapers from the wallpaper management setup.

        Implement this for your wallpaper management setup and use the corresponding subclass.

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError


class Feh(WallpaperManagement):
    """
    Wallpaper management done with feh.
    """
    def __self__(self, wallpaper_directory, monitor_count):
        super().__init__(wallpaper_directory, monitor_count)

    def get_current_wallpapers(self, monitor_count=None):
        if monitor_count is None:
            raise ValueError

        wallpapers_string = None
        with open(os.path.join(os.path.expanduser("~"), ".fehbg"), "r") as f:
            wallpapers_string = f.readlines()[1]  # first line = hashbang, second line = current wallpapers

        wallpapers_string = wallpapers_string.strip()
        wallpapers_string = re.sub(r'feh .*?\'', "", wallpapers_string)

        current_wallpapers = [
            element
            for element
            in wallpapers_string.split("' '")
            if self.wallpaper_directory in element
        ][:self.monitor_count]
        return current_wallpapers

